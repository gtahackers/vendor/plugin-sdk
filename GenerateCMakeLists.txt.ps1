$fileName = "CMakeLists.txt"
$filePath = (Join-Path -Path $PSScriptRoot -ChildPath $fileName)
$rootPath = (Split-Path -Path $filePath -Parent)
$rootPathLength = (Split-Path -Path $filePath -Parent).Length

function WriteTargetSourceFiles {
    Param(
        # Directory path with source files
        [Parameter(Position=0)]
        [string[]]
        $Path,
        [Parameter(Position=1)]
        [bool]
        $Recurse = $false
    )

    Get-ChildItem -Recurse:$Recurse -File -Path:$Path |
    ForEach-Object {
        $directoryName = $_.Directory.FullName.Substring($rootPathLength + 1).Replace("\", "/")
        $fileName = $_.Name;
        Out-File -FilePath $filePath -Append -Encoding ASCII -InputObject "        `"`${CMAKE_CURRENT_LIST_DIR}/$directoryName/$fileName`""
    }
}

# Create new CMakeLists.txt
New-Item -Path $filePath -ItemType "file" -Force > $null

# Switch to plugin-sdk directory to use relative paths for PowerShell functions
Push-Location -LiteralPath $rootPath

# Write CMake header
Out-File -FilePath $filePath -Append -Encoding ASCII -InputObject @'
cmake_minimum_required(VERSION 3.11)

# Source: https://github.com/DK22Pac/plugin-sdk/tree/af4dcd3041a4c5350c1292fa8495489de27d5ca0
add_library(plugin-sdk STATIC)
target_compile_features(plugin-sdk PUBLIC cxx_std_17)
target_compile_options(plugin-sdk PUBLIC /wd4073 /wd4201 /wd4458)
# target_link_libraries(plugin-sdk PRIVATE dxsdk-2009-august)

target_compile_definitions(plugin-sdk
    PUBLIC
        "WIN32_LEAN_AND_MEAN"
        "_CRT_SECURE_NO_WARNINGS"
        "_CRT_NON_CONFORMING_SWPRINTFS"
        "_USING_V110_SDK71_"
        "GTASA"
        "PLUGIN_SGV_10US"
)

target_sources(plugin-sdk
    PRIVATE
'@

# Write source file paths
WriteTargetSourceFiles "injector", "shared", "shared/game"
WriteTargetSourceFiles "plugin_sa/game_sa" $true

# Write CMake footer
Out-File -FilePath $filePath -Append -Encoding ASCII -InputObject @'
)

target_include_directories(plugin-sdk
    PUBLIC
        "${CMAKE_CURRENT_LIST_DIR}/shared"
        "${CMAKE_CURRENT_LIST_DIR}/plugin_sa"
        "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa" # because of shared/Color.h include of CRGBA.h
)

source_group("Injector" REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/injector/*")

source_group("Shared" REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/shared/*")

source_group("game_sa\\Entity"          REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/(CEntity|CBuilding|CPhysical|CPlaceable|CTreadable|CAnimatedBuilding)\\..*")
source_group("game_sa\\Entity\\Dummy"   REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/CDummy(Ped|Object)?\\..*")
source_group("game_sa\\Entity\\Object"  REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/(C(Cutscene|Hand)?Object|CProjectile|CCutsceneHead)\\..*")
source_group("game_sa\\Entity\\Ped"     REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/(C(Civilian|Cop|Emergency|Player)?Ped)\\..*")
source_group("game_sa\\Entity\\Vehicle" REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/(CVehicle|CBike|CBmx|CBoat|CHeli|CMonsterTruck|CPlane|CQuadBike|CTrailer|CTrain)\\..*")
source_group("game_sa\\Animation"       REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/C?Anim.*")
source_group("game_sa\\Audio"           REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/CAE.*")
source_group("game_sa\\Collision"       REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/(CCol[A-Z]|CBox|CLines|CSphere|tColSurface|CBoundingBox).*")
source_group("game_sa\\Core"            REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/(CKeyGen|CLink|CMatrix|COctTree|CPool|CPtr(List|Node)|CQua(dTreeNode|ternion)|CRect|CStore\\.|CVector|List_c|ListItem_c|SArray).*")
source_group("game_sa\\Enums"           REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/e.*")
source_group("game_sa\\Tasks"           REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/CTask(Complex|Manager|Simple|Timer)?\\..*")
source_group("game_sa\\Tasks\\Types"    REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/CTask(Gang|GoTo|Interior|Lean|Simple|Complex)[^\\.].*")
source_group("game_sa\\Scripts"         REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/C(Running|The)Script[s]?\\..*")
source_group("game_sa\\Fx"              REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/Fx.*")
source_group("game_sa\\Models"          REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/.*ModelInfo\\..*")
source_group("game_sa\\Plugins"         REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/(JPegCompress|NodeName|PipelinePlugin)\\..*")
source_group("game_sa\\RenderWare"      REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/(RenderWare\\.|D3D).*")
source_group("game_sa\\RenderWare\\rw"  REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/rw/.*")
source_group("game_sa\\Meta"            REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/meta/.*")
source_group("game_sa"                  REGULAR_EXPRESSION "${CMAKE_CURRENT_LIST_DIR}/plugin_sa/game_sa/.*")

'@

Pop-Location
